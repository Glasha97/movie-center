package com.moviecenter.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

abstract class BaseActivity: AppCompatActivity{

    constructor() : super()

    constructor(contentLayoutId: Int) : super(contentLayoutId)

    protected fun setupNavigation(
        host: NavHostFragment,
        @NavigationRes navigation: Int,
        startDestinationArgs: Bundle? = null
    ): NavController {
        val inflater = host.navController.navInflater
        val graph = inflater.inflate(navigation)
        host.navController.setGraph(graph, startDestinationArgs)
        return host.navController
    }

    protected fun setupNavigation(
        host: NavHostFragment,
        @NavigationRes navigation: Int,
        @IdRes startDestination: Int,
        startDestinationArgs: Bundle? = null
    ): NavController {
        val inflater = host.navController.navInflater
        val graph = inflater.inflate(navigation)
        graph.startDestination = startDestination
        host.navController.setGraph(graph, startDestinationArgs)
        return host.navController
    }
}
package com.moviecenter.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<VDB : ViewDataBinding> : Fragment() {

    protected lateinit var binding: VDB
    protected abstract fun layout(): Int

//    private var progressDialog: ProgressDialog? = null
//    private var generalErrorDialog: GeneralDialog? = null
//    private var networkErrorDialog: NetworkErrorDialog? = null
//    private val dialogFragmentManager: FragmentManager by lazy { childFragmentManager }

    protected open fun onCreateView(savedInstanceState: Bundle?) {}
    protected var toolbar: ActionBar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layout(), container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        toolbar = (requireActivity() as AppCompatActivity).supportActionBar
        onCreateView(savedInstanceState)

        return binding.root
    }

//    fun showProgressDialog(
//        message: String? = null,
//        onBackClickListener: ((dialog: DialogInterface?) -> Unit)? = null
//    ): ProgressDialog? {
//        hideErrorDialog()
//        hideNetworkErrorDialog()
//        if (progressDialog?.isVisible == true) {
//            //   progressDialog?.setMessageWhenShowed(message)
//            progressDialog?.onBackClickListener = onBackClickListener
//            return progressDialog
//        }
//        // progressDialog = ProgressDialog.newInstance(message)
//        progressDialog?.onBackClickListener = onBackClickListener
//
//        try {
//            if (progressDialog?.isVisible == false) progressDialog?.showNow(
//                dialogFragmentManager,
//                ProgressDialog::class.java.simpleName
//            )
//        } catch (e: IllegalStateException) {
//            Log.e(BaseFragment::class.simpleName, "showProgressDialog", e)
//        }
//        return progressDialog
//    }
//
//    fun hideProgressDialog() {
//        progressDialog?.dismiss()
//    }
//
//    fun showErrorDialog(
//        tittle: String? = null,
//        message: String? = null,
//        buttonTittle: String? = null,
//        onBackOrBtnClickListener: ((
//            dialog: DialogInterface?
//        ) -> Unit)? = null
//    ) {
//        hideProgressDialog()
//        hideNetworkErrorDialog()
//        if (generalErrorDialog?.isVisible == true) {
////            generalErrorDialog?.setTitleWhenShowed(tittle)
////            generalErrorDialog?.setMessageWhenShowed(message)
////            generalErrorDialog?.setButtonText(buttonTittle)
//            generalErrorDialog?.onBackOrBtnClickListener = onBackOrBtnClickListener
//            return
//        }
//        //generalErrorDialog = GeneralDialog.newInstance(tittle, message, buttonTittle)
//        generalErrorDialog?.onBackOrBtnClickListener = onBackOrBtnClickListener
//
//        try {
//            if (generalErrorDialog?.isVisible == false) generalErrorDialog?.showNow(
//                dialogFragmentManager,
//                GeneralDialog::class.java.simpleName
//            )
//        } catch (e: IllegalStateException) {
//            Log.e(BaseFragment::class.simpleName, "showErrorDialog", e)
//        }
//    }
//
//    fun hideErrorDialog() {
//        if (generalErrorDialog?.isVisible == true) {
//            generalErrorDialog?.dismiss()
//        }
//    }
//
//    fun showNetworkErrorDialog(
//        btnText: String? = null,
//        message: String? = null,
//        onBackClickListener: ((dialog: DialogInterface?) -> Unit)? = null,
//        onBtnClickListener: ((dialog: DialogInterface?) -> Unit)? = null
//    ) {
//        hideProgressDialog()
//        hideErrorDialog()
////        networkErrorDialog = NetworkErrorDialog.newInstance(message, btnText)
////        networkErrorDialog?.onBackClickListener = onBackClickListener
////        networkErrorDialog?.onBtnClickListener = onBtnClickListener
//
//        try {
//            if (networkErrorDialog?.isVisible == false) networkErrorDialog?.showNow(
//                dialogFragmentManager,
//                NetworkErrorDialog::class.java.simpleName
//            )
//        } catch (e: IllegalStateException) {
//            Log.e(BaseFragment::class.simpleName, "showNetworkErrorDialog", e)
//        }
//    }
//
//    fun hideNetworkErrorDialog() {
//        if (networkErrorDialog?.isVisible == true) {
//            networkErrorDialog?.dismiss()
//        }
//    }
}
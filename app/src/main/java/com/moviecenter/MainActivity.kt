package com.moviecenter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.moviecenter.R
import com.moviecenter.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigation(
            supportFragmentManager.findFragmentByTag(getString(R.string.nav_host_fragment_tag)) as NavHostFragment,
            R.navigation.main
        )
    }
}
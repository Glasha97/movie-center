package com.moviecenter.main.last_movies

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.moviecenter.R
import com.example.moviecenter.databinding.FragmentLastMoviesBinding
import com.moviecenter.base.fragment.BaseFragment

class LastMoviesFragment : BaseFragment<FragmentLastMoviesBinding>() {
    override fun layout(): Int = R.layout.fragment_last_movies

    override fun onCreateView(savedInstanceState: Bundle?) {
        super.onCreateView(savedInstanceState)
        binding.btn.setOnClickListener {
            val shared = FragmentNavigatorExtras(binding.btn to "test")
            findNavController().navigate(
                LastMoviesFragmentDirections.toSelectedMovieFragment(), shared
            )
        }
    }
}
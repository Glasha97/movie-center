package com.moviecenter.main.last_movies.selected_movie

import android.os.Bundle
import androidx.transition.TransitionInflater
import com.example.moviecenter.R
import com.example.moviecenter.databinding.FragmentSelectedMovieBinding
import com.moviecenter.base.fragment.BaseFragment

class SelectedMovieFragment : BaseFragment<FragmentSelectedMovieBinding>() {
    override fun layout(): Int = R.layout.fragment_selected_movie

    override fun onCreateView(savedInstanceState: Bundle?) {
        super.onCreateView(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.move)
    }
}